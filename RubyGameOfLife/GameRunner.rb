# Runner for a GameBoard
class GameRunner
  def initialize(game_board)
    @game_board = game_board
  end
  
  # Run the game for the specified number of iterations
  # with the specified delay between ticks in seconds
  def run (iteration, delay)
      for i in 0..iteration
          system ("clear")
          @game_board.print_board
          @game_board.tick
          sleep delay
      end
  end
end