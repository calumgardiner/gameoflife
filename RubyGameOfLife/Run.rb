require_relative './GameRunner'
require_relative './GameBoard'

def usage
    print "usage: ruby Run.rb {columns}, {rows}, {iterations}, {delay} \n"
end

if (ARGV.size < 4 or ARGV.size > 4)
    usage
    exit
end
    
cols = ARGV[0].to_i
rows = ARGV[1].to_i
iterations = ARGV[2].to_i
delay = ARGV[3].to_i

game_board = GameBoard.new(cols, rows)
game_board.randomize
game_runner = GameRunner.new(game_board)
game_runner.run(iterations, delay)