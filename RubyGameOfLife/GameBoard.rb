# Game of Life board
class GameBoard

# Board initializes to all dead cells by default
def initialize(cols, rows)
    @cols = cols
    @rows = rows
    @matrix = Array.new(rows) {Array.new(cols,:dead)}
end

# Print the board in an easy to view syntax
def print_board
    @matrix.each { |a| 
        for i in 0..a.size-1
            cell = (a[i] == :dead ? "  " : "[]")
            print (i == a.size-1 ? cell + "\n" : cell)
        end
    }
end

# Change the status of the cell x,y
def set_cell(x, y, status)
    @matrix[x][y] = status
end

# Get an array of the given cells neighbours
# Uses toroidal array at edges
def get_neighbours(x, y)
    xminus1 = (x == 0 ? @cols - 1 : x - 1)
    yminus1 = (y == 0 ? @rows - 1 : y - 1)
    xplus1  = (x == (@cols - 1) ? 0 : x + 1)
    yplus1  = (y == (@rows - 1) ? 0 : y + 1)
    neighbours = Array.new
    neighbours << @matrix[xminus1][yminus1]
    neighbours << @matrix[x][yminus1]
    neighbours << @matrix[xplus1][yminus1]
    neighbours << @matrix[xminus1][y]
    neighbours << @matrix[xplus1][y]
    neighbours << @matrix[xminus1][yplus1]
    neighbours << @matrix[x][yplus1]
    neighbours << @matrix[xplus1][yplus1]
end

# Do one tick of the game
def tick
    matrixcopy = @matrix.clone
    for x in 0..@cols-1
        for y in 0..@rows-1
            neighbours = get_neighbours(x, y)
            alive_neighbours =  neighbours.select { |cell| cell == :alive }.size
            if alive_neighbours < 2
                matrixcopy[x][y] = :dead
            elsif alive_neighbours == 2 and @matrix[x][y] == :alive
                matrixcopy[x][y] = :alive
            elsif alive_neighbours == 3
                matrixcopy[x][y] = :alive
            else
                matrixcopy[x][y] = :dead
            end
        end
    end
    @amtrix  = matrixcopy
end

# Set all values to dead
def kill_all
    @matrix.each { |a| 
        a.fill(:dead)
    }
end

# Set all values to alive
def life_all
    @matrix.each { |a| 
        a.fill(:alive)
    }
end

# Randomise
def randomize
    for x in 0..@cols-1
        for y in 0..@rows-1
            @matrix[x][y] = (rand > 0.5 ? :alive : :dead)
        end
    end
end

private :get_neighbours
end